package com.kfwebstandard.beans;

import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.RequestScoped;

import javax.inject.Named;

/**
 * Backing bean for index.xhtml
 *
 * @author Ken Fogel
 */
@Named("payment")
@RequestScoped
public class PaymentBean implements Serializable {

    private double amount;
    private String card = "";
    private Date date = new Date();

    public void setAmount(double newValue) {
        amount = newValue;
    }

    public double getAmount() {
        return amount;
    }

    public void setCard(String newValue) {
        card = newValue;
    }

    public String getCard() {
        return card;
    }

    public void setDate(Date newValue) {
        date = newValue;
    }

    public Date getDate() {
        return date;
    }
}
